-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2017 at 07:07 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `birthDate` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birthDate`, `softDelete`) VALUES
(1, 'Sumon', '1997-07-18', 'Yes'),
(5, 'AB Akas', '1997-11-20', 'No'),
(8, 'AB Akas', '1997-11-20', 'No'),
(9, 'AB Akas', '2017-02-25', 'Yes'),
(11, 'sagla', '2017-02-23', 'No'),
(12, 'Samrat Barua', '2000-07-08', 'Yes'),
(13, 'leda poa', '2017-02-11', 'Yes'),
(14, 'Paglu', '2017-02-15', 'Yes'),
(15, 'Saglu', '2017-02-10', 'No'),
(16, 'Montu', '2017-02-10', 'No'),
(17, 'Lover Boy Sumon', '2012-07-06', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(7, 'AB', 'ABAkas', 'Yes'),
(14, 'Story', 'Sumon', 'Yes'),
(15, 'AB', 'AB Akas', 'Yes'),
(16, 'Story', 'Sumon', 'Yes'),
(19, 'AB', 'ABAkas', 'Yes'),
(20, 'mn,dnv', 'jsdnkv', 'No'),
(21, 'mn,dnv', 'jsdnkv', 'No'),
(23, 'mn,dnv', 'jsdnkv', 'No'),
(27, 'ab', 'AB', 'No'),
(28, 'AB', 'ABAkas', 'No'),
(30, 'AB', 'ABAkas', 'No'),
(31, 'Bangla', 'Kono ek lekok', 'No'),
(32, 'Shurjha', 'Jani na', 'No'),
(33, 'Himaloy Barua', 'AB Akas', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `cityName` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `cityName`, `softDelete`) VALUES
(1, 'AB Akas', 'Chittagong', 'No'),
(4, 'Pagla', 'Chittagong', 'No'),
(6, 'Saglu', 'Khulna', 'No'),
(7, 'Samrat', 'Dhaka', 'Yes'),
(8, 'Sumon', 'Jossore', 'Yes'),
(9, 'Sagu', 'Chittagong', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `softDelete`) VALUES
(1, 'Samrat', 'Samrat123@gmail.com', 'Yes'),
(2, 'Sumon', 'sumon@gmail.com', 'Yes'),
(3, 'Sagu', 'Sagu@yahoo.com', 'Yes'),
(4, 'Pagla', 'pagla@gmail.com', 'Yes'),
(5, 'Saglu', 'saglu@gmail.com', 'No'),
(7, 'Anowar', 'Anowar@gmail.com', 'Yes'),
(11, 'Pagla', 'pagla@gmail.com', 'No'),
(12, 'AB Akas', 'ab.akas@yahoo.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `softDelete`) VALUES
(1, 'Pagli', 'Female', 'No'),
(4, 'Sumon', 'Male', 'Yes'),
(5, 'Sumon', 'Male', 'No'),
(6, 'sdfasdf', 'Male', 'No'),
(7, 'asdfasdf', 'Female', 'No'),
(8, 'asdfasdfasdf', 'Female', 'No'),
(9, 'asdfasdfasdgasdf', 'Male', 'No'),
(10, 'asdfasdfsadf', 'Female', 'Yes'),
(11, 'asdfasdfsadf', 'Female', 'Yes'),
(12, 'sdfasdfasdf', 'Male', 'Yes'),
(13, 'asdfasdsdfgbzxg', 'Male', 'No'),
(14, 'asdfgasdg', 'Female', 'Yes'),
(15, 'asdgasdgsdg', 'Male', 'No'),
(16, 'asdgasdgsadfg', 'Male', 'No'),
(17, 'asdgsadgsadg', 'Male', 'No'),
(18, 'sadgsadgsag', 'Male', 'No'),
(19, 'sdbfstgb', 'Male', 'No'),
(20, 'm,jgidfg s', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `softDelete`) VALUES
(1, 'Sumon', 'Reading,Drawing.', 'No'),
(2, 'shantu', 'Reading,Gardening,Singing.', 'No'),
(4, 'Paglu', 'Reading,Drawing,Swimming,Gardening,Singing.', 'No'),
(5, 'dfsdfgsfdg', 'Reading,Drawing.', 'Yes'),
(6, 'dsfgsdfgsdfg', 'Drawing,Swimming,Gardening.', 'Yes'),
(7, 'sdfgsdfgsdfg', 'Reading,Swimming,Gardening.', 'Yes'),
(8, 'sdfgsdfgsdfg', 'Drawing,Swimming,Gardening,Singing.', 'Yes'),
(9, 'sdfgabnsgsdg', 'Reading,Drawing,Swimming,Gardening,Singing.', 'Yes'),
(10, 'asdf', 'Reading,Drawing,Swimming,Gardening,Singing.', 'Yes'),
(11, 'asdfasdf', 'Drawing.', 'No'),
(12, 'sdfasdfasdf', 'Swimming.', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `pictureName` varchar(111) NOT NULL,
  `tmpLocation` varchar(111) NOT NULL,
  `type` varchar(111) NOT NULL,
  `size` int(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `pictureName`, `tmpLocation`, `type`, `size`, `softDelete`) VALUES
(18, 'AB Akas', 'AB Akas_1486360198.jpeg', 'C:\\xampp\\tmp\\php8EDF.tmp', 'image/jpeg', 264, 'Yes'),
(19, 'jkashd', 'jkashd_1486360218.jpeg', 'C:\\xampp\\tmp\\phpDEB6.tmp', 'image/jpeg', 429, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `description` text NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `description`, `softDelete`) VALUES
(1, 'AB Akas', 'asdjjnlnbh s ahsdvhd  hjaohohsahwkjnhahsdkd sasdijo dv', 'Yes'),
(2, 'sdnlkasd', 'and;kvnoasd;nvosdv', 'Yes'),
(3, 'asd;n;kasndov', 'asdnosadovn', 'No'),
(4, 'asdf''asdfosdnfasdnf''', 'ansdfnsdnfo', 'No'),
(5, 'asnd;knsdonf', 'asdnfsdjoifnsdf', 'No'),
(6, 'sdnfo''sndofn', 'adsnolfjnsodf', 'No'),
(7, 'sandofnap''sdfp', 'asdnf''opsjnpdf', 'Yes'),
(8, 'asdnlfnaspd''f', 'adsnfonpdof', 'Yes'),
(9, 'asdnflnasdp''ofj', 'asdn''fsnjdpf', 'Yes'),
(10, 'sajdl''fsdfnm', 'asdj''pF''asdf', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
