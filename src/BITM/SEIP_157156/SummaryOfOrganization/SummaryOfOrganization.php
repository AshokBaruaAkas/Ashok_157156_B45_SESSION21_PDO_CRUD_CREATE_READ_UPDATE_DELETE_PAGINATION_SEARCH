<?php

namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class SummaryOfOrganization extends DB{
    private $id;
    private $name;
    private $description;
    private $btnValue;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("description",$allPostData)){
            $this->description = $allPostData['description'];
        }
        if(array_key_exists("btn",$allPostData)){
            $this->btnValue = $allPostData['btn'];
        }
    }

    public function store()
    {
        $arrayData = array($this->name,$this->description);
        $query = 'INSERT INTO summary_of_organization (name, description) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('create.php');
        }
        else{
            Utility::redirect('read.php');
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM summary_of_organization WHERE softDelete = 'No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view()
    {
        $sql = "SELECT * FROM summary_of_organization WHERE id = '".$this->id."'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update()
    {
        $arrayData = array($this->name,$this->description,$this->id);
        $query = 'UPDATE summary_of_organization SET name = ?, description = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Updated!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Updated!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('read.php');
        }
        else{
            Utility::redirect('trashed.php');
        }
    }

    public function trash()
    {
        $arrayData = array("Yes",$this->id);
        $query = 'UPDATE summary_of_organization SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Trashed!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!.");
        }

        Utility::redirect('read.php');
    }

    public function delete()
    {
        $arrayData = array($this->id);
        $query = "DELETE FROM summary_of_organization WHERE id = '".$this->id."'";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted Permanently.!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Delete!.");
        }

        if($this->btnValue == "set")   Utility::redirect('read.php');
        else Utility::redirect('trashed.php');
    }

    public function trashed()
    {
        $sql = "select * from summary_of_organization WHERE softDelete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function recover()
    {
        $arrayData = array("No",$this->id);
        $query = 'UPDATE summary_of_organization SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!.");
        }

        Utility::redirect('trashed.php');
    }

    public function indexPagination($page=1, $itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary_of_organization  WHERE softDelete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPagination($page=1, $itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary_of_organization  WHERE softDelete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
}