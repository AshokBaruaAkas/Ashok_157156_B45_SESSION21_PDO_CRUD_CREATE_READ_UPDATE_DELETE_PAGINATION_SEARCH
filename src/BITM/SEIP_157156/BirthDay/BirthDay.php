<?php

namespace App\BirthDay;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BirthDay extends DB
{
    private $id;
    private $name;
    private $birthDate;
    private $btnValue;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("birthDate",$allPostData)){
            $this->birthDate = $allPostData['birthDate'];
        }
        if(array_key_exists("btn",$allPostData)){
            $this->btnValue = $allPostData['btn'];
        }
    }

    public function store()
    {
        $arrayData = array($this->name,$this->birthDate);
        $query = 'INSERT INTO birth_day (name, birthDate) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('create.php');
        }
        else{
            Utility::redirect('read.php');
        }
    }

    public function index(){
        $query = "SELECT * FROM birth_day WHERE softDelete = 'No'";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view()
    {
        $sql = "SELECT * FROM birth_day WHERE id = '".$this->id."'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update()
    {
        $arrayData = array($this->name,$this->birthDate,$this->id);
        $sql = "UPDATE birth_day SET name = ?, birthDate = ? WHERE id = ?";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Updated!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Updated!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('read.php');
        }
        else{
            Utility::redirect('trashed.php');
        }
    }

    public function trash()
    {
        $arrayData = array("Yes",$this->id);
        $query = 'UPDATE birth_day SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Trashed!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!.");
        }

        Utility::redirect('read.php');
    }

    public function trashed()
    {
        $sql = "select * from birth_day WHERE softDelete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function recover()
    {
        $arrayData = array("No",$this->id);
        $query = 'UPDATE birth_day SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!.");
        }

        Utility::redirect('trashed.php');
    }

    public function delete()
    {
        $arrayData = array($this->id);
        $query = "DELETE FROM birth_day WHERE id = ?";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted Permanently.!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Delete!.");
        }

        if($this->btnValue == "set")   Utility::redirect('read.php');
        else Utility::redirect('trashed.php');
    }

    public function indexPagination($page=1, $itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from birth_day  WHERE softDelete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPagination($page=1, $itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from birth_day  WHERE softDelete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
}