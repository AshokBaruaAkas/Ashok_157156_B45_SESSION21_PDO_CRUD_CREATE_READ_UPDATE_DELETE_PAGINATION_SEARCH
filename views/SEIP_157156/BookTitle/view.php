<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\BookTitle\BookTitle;

$objBookTitle->setData($_GET);

$oneData = $objBookTitle->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Book Title - Single Book Information</h1>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <div class="row col-md-3"></div>
            <div class="row col-md-7">
                <table class="table table-bordered table table-striped">
                    <tr>
                        <th width="30%">Name</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <td>ID:</td>
                        <td><?php echo $oneData->id;?></td>
                    </tr>
                    <tr>
                        <td>Book Name:</td>
                        <td><?php echo $oneData->book_name;?></td>
                    </tr>
                    <tr>
                        <td>Author Name:</td>
                        <td><?php echo $oneData->author_name;?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
</body>
</html>