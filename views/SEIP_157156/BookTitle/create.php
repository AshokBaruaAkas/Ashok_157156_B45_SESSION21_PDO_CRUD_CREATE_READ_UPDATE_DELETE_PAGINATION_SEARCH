<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($msg)){
        $msg = Message::getMessage();
    }
    else{
        $msg = "";
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title Collection Form</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Book Title Collection Form</h1>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation" class="active"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <form action="store.php" method="post" class="form text-center">
                <div class="form-group">
                    <label for="bookName">Enter Book Name</label>
                    <input type="text" class="form-control text-center" name="bookName" placeholder="Type Book Name Here" required>
                </div>
                <div class="form-group">
                    <label for="authorName">Enter Author Name</label>
                    <input type="text" class="form-control text-center" name="authorName" placeholder="Type Author Name Here" required>
                </div>
                <button type="submit" class="btn btn-lg btn-primary" name="btn" value="set">Save</button>
                <button type="submit" class="btn btn-lg btn-primary" name="btn" value="">Save & View</button>
            </form>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>