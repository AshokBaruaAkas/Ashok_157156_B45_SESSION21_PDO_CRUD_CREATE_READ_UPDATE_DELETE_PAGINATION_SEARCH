<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($msg)){
        $msg = Message::getMessage();
    }
    else{
        $msg = "";
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City Information Collection Form</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>City Information Collection Form</h1>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation" class="active"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <form action="store.php" method="post" class="form text-center">
                <div class="form-group">
                    <label for="name">Enter Your Name</label>
                    <input type="text" class="form-control text-center" name="name" placeholder="Type Your Name Here" required>
                </div>
                <div class="selectPenal">
                    <select class="form-control" name="city" required>
                        <option selected>Choice Your City</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Khulna">Khulna</option>
                        <option value="Rajshahi">Rajshahi</option>
                        <option value="Comilla">Comilla</option>
                        <option value="Rongpur City">Rongpur City</option>
                        <option value="Tongi">Tongi</option>
                        <option value="Norshingdi">Norshingdi</option>
                        <option value="Cox's Bazar">Cox's Bazar</option>
                        <option value="Jossore">Jossore</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-lg btn-primary" name="btn" value="set">Save</button>
                <button type="submit" class="btn btn-lg btn-primary" name="btn" value="">Save & View</button>
            </form>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>