<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Atomic Project</title>
    <link rel="stylesheet" href="../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1 class="homeHead">ATOMIC PROJECT</h1>
    </div>
    <div class="site_body">
        <div class="all-item">
                <div class="book-title project-block">
                    <h3>Book Title</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/booktitle.png" alt="">
                    </div>
                    <a href="BookTitle/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="BookTitle/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="birth-day project-block">
                    <h3>Birth Day</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/birthday.png" alt="">
                    </div>
                    <a href="BirthDay/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="BirthDay/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="city project-block">
                    <h3>City</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/city.png" alt="">
                    </div>
                    <a href="City/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="City/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="email project-block">
                    <h3>E-mail</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/email.png" alt="">
                    </div>
                    <a href="Email/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="Email/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="gender project-block">
                    <h3>Gender</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/gender.png" alt="">
                    </div>
                    <a href="Gender/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="Gender/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="hobbies project-block">
                    <h3>Hobbies</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/hobbies.png" alt="">
                    </div>
                    <a href="Hobbies/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="Hobbies/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="profile-picture project-block">
                    <h3>Profile Picture</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/profilepicture.png" alt="">
                    </div>
                    <a href="ProfilePicture/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="ProfilePicture/read.php" class="btn-sm btn-info">Record List</a>
                </div>
                <div class="summary-of-organization project-block">
                    <h3>Organization</h3>
                    <div class="img center-block">
                        <img src="../../resource/images/summaryoforganization.png" alt="">
                    </div>
                    <a href="SummaryOfOrganization/create.php" class="btn-sm btn-success">Add New</a>
                    <a href="SummaryOfOrganization/read.php" class="btn-sm btn-info">Record List</a>
                </div>
        </div>
        <div class="description">
            <div class="Side">
                <p>Student Name:</p>
                <p>SEIP:</p>
                <p>Project Name:</p>
                <p>Batch No:</p>
            </div>
            <div class="Side"></div>
            <p>Ashok Barua.</p>
            <p>157156.</p>
            <p>Atomic Project.</p>
            <p>45.</p>
        </div>
    </div>
</div>
</body>
</html>