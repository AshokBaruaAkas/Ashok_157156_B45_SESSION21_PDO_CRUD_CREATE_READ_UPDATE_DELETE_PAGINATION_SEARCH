<?php
require_once("../../../vendor/autoload.php");

use \App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();

$objSummaryOfOrganization->setData($_POST);

$objSummaryOfOrganization->store();