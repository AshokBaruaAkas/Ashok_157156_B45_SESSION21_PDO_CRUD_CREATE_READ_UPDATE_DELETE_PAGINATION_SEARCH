<?php
require_once("../../../vendor/autoload.php");

use \App\ProfilePicture\ProfilePicture;
use App\Utility\Utility;
use App\Message\Message;

$objProfilePicture = new ProfilePicture();

$objProfilePicture->setData($_POST);

$objProfilePicture->setProfilePicture($_FILES);

if($objProfilePicture->pictureTypeArray[0] == "image"){
    $objProfilePicture->store();

    $objProfilePicture->storePicture();
}
else{
    Message::setMessage("Please! Select Image Only.");
    Utility::redirect('create.php');
}