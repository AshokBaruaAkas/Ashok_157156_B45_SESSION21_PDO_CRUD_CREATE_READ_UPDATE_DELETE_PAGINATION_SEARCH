<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_GET);

$oneData = $objProfilePicture->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture - Single Profile Picture Information</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Single Profile Picture Information</h1>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-7 viewTable">
                    <table class="table table-bordered table table-striped">
                        <tr>
                            <th width="30%">Name</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>ID:</td>
                            <td><?php echo $oneData->id;?></td>
                        </tr>
                        <tr>
                            <td>User Name:</td>
                            <td><?php echo $oneData->name;?></td>
                        </tr>
                        <tr>
                            <td>Picture Name:</td>
                            <td><?php echo $oneData->pictureName;?></td>
                        </tr>
                        <tr>
                            <td>Temp Location:</td>
                            <td><?php echo $oneData->tmpLocation;?></td>
                        </tr>
                        <tr>
                            <td>Type:</td>
                            <td><?php echo $oneData->type;?></td>
                        </tr>
                        <tr>
                            <td>Size:</td>
                            <td><?php echo $oneData->size." KB";?></td>
                        </tr>
                    </table>
                </div>
                <div class="showImage col-md-5">
                    <h2>Profile Picture</h2>
                    <img src="UploadedPicture/<?php echo $oneData->pictureName;?>" alt="">
                </div>
            </div>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
</body>
</html>