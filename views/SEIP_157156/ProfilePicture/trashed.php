<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture;
$allData = $objProfilePicture->trashed();

use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objProfilePicture->trashedPagination($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture - Trashed List</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Profile Picture - Trashed List</h1>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation" class="active"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <table class="table table-bordered table-striped">
                <tr>
                    <th width="6%">Sl No</th>
                    <th width="5%">ID</th>
                    <th>User Name</th>
                    <th>Picture Name</th>
                    <th>Type</th>
                    <th>Size</th>
                    <th width="25%">Action Buttons</th>
                </tr>
                <?php
                foreach($someData as $oneData){
                    echo "
                        <tr>
                            <td>$serial</td>
                            <td>$oneData->id</td>
                            <td>$oneData->name</td>
                            <td>$oneData->pictureName</td>
                            <td>$oneData->type</td>
                            <td>$oneData->size KB</td>
                            <td>
                                <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                <a href='edit.php?id=$oneData->id' class='btn btn-success'>Edit</a>
                                <a href='recover.php?id=$oneData->id' class='btn btn-warning'>Recover</a>
                                <a href='delete.php?id=$oneData->id&pictureName=$oneData->pictureName' class='btn btn-danger' onclick='return confirm(\"Are You Sure?\")'>Delete</a>
                            </td>
                        </tr>
                    ";
                    $serial++;
                }
                ?>
            </table>
            <!--  ######################## pagination code block#2 of 2 start ###################################### -->
            <div align="center" >
                <ul class="pagination">

                    <?php

                    $pageMinusOne  = $page-1;
                    $pagePlusOne  = $page+1;
                    if($page>$pages) Utility::redirect("trashed.php?Page=$pages");

                    if($page>1)  echo "<li><a href='trashed.php?Page=$pageMinusOne'><span aria-hidden='true'>" . "&laquo;" . "</span></a></li>";
                    for($i=1;$i<=$pages;$i++)
                    {
                        if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                        else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                    }
                    if($page<$pages) echo "<li><a href='trashed.php?Page=$pagePlusOne'><span aria-hidden='true'>" . "&raquo;" . "</span></a></li>";

                    ?>
                </ul>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                            <?php
                            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <!--  ######################## pagination code block#2 of 2 end ###################################### -->
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>