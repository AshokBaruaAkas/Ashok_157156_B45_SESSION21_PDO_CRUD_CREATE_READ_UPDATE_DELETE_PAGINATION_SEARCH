<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_POST);

$objProfilePicture->setProfilePicture($_FILES);

$objProfilePicture->update();